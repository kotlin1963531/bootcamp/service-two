package partners.bootcamp.client

import partners.bootcamp.model.enumerable.FileStatus
import jakarta.inject.Singleton
import java.io.InputStream

@Singleton
class MockAntivirusClient : AntivirusClient {
    override suspend fun check(file: InputStream): FileStatus {
        return FileStatus.entries.toTypedArray().filter { it != FileStatus.ERROR }.random()
    }
}
