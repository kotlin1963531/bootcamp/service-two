package partners.bootcamp.client

import partners.bootcamp.model.enumerable.FileStatus
import java.io.InputStream

interface AntivirusClient {
    suspend fun check(file: InputStream): FileStatus
}
