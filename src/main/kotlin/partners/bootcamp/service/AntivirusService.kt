package partners.bootcamp.service

import partners.bootcamp.client.AntivirusClient
import partners.bootcamp.integration.event.FileStatusChangedEvent
import partners.bootcamp.integration.event.producer.FileStatusChangedProducer
import partners.bootcamp.model.enumerable.FileStatus
import partners.bootcamp.storage.MockFileStorage
import jakarta.inject.Singleton
import java.util.UUID

@Singleton
class AntivirusService(
    private val fileStorage: MockFileStorage,
    private val antivirusClient: AntivirusClient,
    private val fileStatusChangedProducer: FileStatusChangedProducer
) {

    suspend fun scan(fileId: UUID): FileStatus {
        val file = fileStorage.getFile(fileId)

        val status = antivirusClient.check(file)
        fileStatusChangedProducer.send(FileStatusChangedEvent(fileId, status))

        return status
    }
}
