package partners.bootcamp.model.enumerable

import io.micronaut.serde.annotation.Serdeable

@Serdeable
enum class FileStatus {
    NOT_INFECTED, INFECTED, ERROR
}
