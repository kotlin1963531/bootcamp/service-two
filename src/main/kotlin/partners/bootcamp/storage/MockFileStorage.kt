package partners.bootcamp.storage

import jakarta.inject.Singleton
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.nio.charset.StandardCharsets
import java.util.UUID

@Singleton
class MockFileStorage : FileStorage {
    override suspend fun getFile(fileId: UUID): InputStream {
        val dummyContent = "This is a dummy string for the input stream."
        return ByteArrayInputStream(dummyContent.toByteArray(StandardCharsets.UTF_8))
    }
}
