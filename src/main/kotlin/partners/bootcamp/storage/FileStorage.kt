package partners.bootcamp.storage

import java.io.InputStream
import java.util.UUID

interface FileStorage {
    suspend fun getFile(fileId: UUID): InputStream
}
