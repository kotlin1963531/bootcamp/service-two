package partners.bootcamp.integration.event.producer

import partners.bootcamp.integration.event.FileStatusChangedEvent
import partners.bootcamp.integration.event.initializer.FILE_STATUS_CHANGED_EXCHANGE_NAME
import partners.bootcamp.integration.event.initializer.FILE_STATUS_CHANGED_ROUTING_KEY
import io.micronaut.rabbitmq.annotation.Binding
import io.micronaut.rabbitmq.annotation.RabbitClient

@RabbitClient(FILE_STATUS_CHANGED_EXCHANGE_NAME)
interface FileStatusChangedProducer {

    @Binding(FILE_STATUS_CHANGED_ROUTING_KEY)
    suspend fun send(data: FileStatusChangedEvent)
}
