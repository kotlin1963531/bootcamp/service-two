package partners.bootcamp.integration.event.initializer

import com.rabbitmq.client.BuiltinExchangeType
import com.rabbitmq.client.Channel
import io.micronaut.rabbitmq.connect.ChannelInitializer
import jakarta.inject.Singleton

const val FILE_STATUS_CHANGED_EXCHANGE_NAME = "FILE_STATUS_CHANGED_EXCHANGE"
const val FILE_STATUS_CHANGED_QUEUE_NAME = "FILE_STATUS_CHANGED_QUEUE"
const val FILE_STATUS_CHANGED_ROUTING_KEY = "file_status"

@Singleton
class FileStatusChangedChannelInitializer : ChannelInitializer() {

    override fun initialize(channel: Channel, name: String?) {
        val dlqArgs = mapOf(
            "x-dead-letter-exchange" to DLQ_EXCHANGE_NAME,
            "x-dead-letter-routing-key" to DLQ_FILE_STATUS_CHANGED_ROUTING_KEY
        )

        channel.exchangeDeclare(FILE_STATUS_CHANGED_EXCHANGE_NAME, BuiltinExchangeType.DIRECT)
        channel.queueDeclare(FILE_STATUS_CHANGED_QUEUE_NAME, true,  false, false, dlqArgs)
        channel.queueBind(FILE_STATUS_CHANGED_QUEUE_NAME, FILE_STATUS_CHANGED_EXCHANGE_NAME, FILE_STATUS_CHANGED_ROUTING_KEY)
    }

}
