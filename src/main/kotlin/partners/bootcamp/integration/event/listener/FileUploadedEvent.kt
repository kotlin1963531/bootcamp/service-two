package partners.bootcamp.integration.event.listener

import partners.bootcamp.integration.event.FileUploadedEvent
import partners.bootcamp.integration.event.FileStatusChangedEvent
import partners.bootcamp.integration.event.initializer.FILE_UPLOADED_QUEUE_NAME
import partners.bootcamp.integration.event.producer.FileStatusChangedProducer
import partners.bootcamp.model.enumerable.FileStatus
import partners.bootcamp.service.AntivirusService
import io.micronaut.rabbitmq.annotation.Queue
import io.micronaut.rabbitmq.annotation.RabbitListener
import io.micronaut.rabbitmq.bind.RabbitAcknowledgement
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

@RabbitListener
class FileUploadedEvent(
    private val antivirusService: AntivirusService,
    private val fileStatusChangedProducer: FileStatusChangedProducer
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO)

    @Queue(FILE_UPLOADED_QUEUE_NAME)
    fun handleFileUploadedEvent(event: FileUploadedEvent, acknowledgement: RabbitAcknowledgement) {
        scope.launch {
            logger.info("FileUploadedEvent.handleRadarDataEvent - File ${event.id} comes for scanning")
            try {
                antivirusService.scan(event.id)
                acknowledgement.ack()
            } catch (e: Exception) {
                logger.error("FileUploadedEvent.handleRadarDataEvent - Error during scanning", e)
                fileStatusChangedProducer.send(FileStatusChangedEvent(event.id, FileStatus.ERROR))
                acknowledgement.nack()
            }
        }
    }
}

